/**********************************************
* File: SeparateChaining.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains the main Hash Table class used for 
* Separate Chaining
**********************************************/
#ifndef SEPARATE_CHAINING_H
#define SEPARATE_CHAINING_H

#include <vector>
#include <list>
#include <string>
#include <algorithm>
#include <functional>
#include <iterator>

#include "SeparateHash.h"

template <typename HashedObj>
class HashTable
{
	
	private:
	
		// Array of Separate Chained Lists 
		std::vector<std::list<HashedObj>> theLists;
		
		// Current Size
		int currentSize;

		/********************************************
		* Function Name  : rehash
		* Pre-conditions :  
		* Post-conditions: none
		*  
		********************************************/
		void rehash( )
		{
			std::vector<std::list<HashedObj>> oldLists = theLists;

			// Create new double-sized, empty table
			theLists.resize( nextPrime( 2 * theLists.size( ) ) );
			for( auto & thisList : theLists )
				thisList.clear( );

			// Copy table over
			currentSize = 0;
			for( auto & thisList : oldLists )
				for( auto & x : thisList )
					insert( std::move( x ) );
		}

		/********************************************
		* Function Name  : myhash
		* Pre-conditions :  const HashedObj & x 
		* Post-conditions: size_t
		*  
		********************************************/
		size_t myhash( const HashedObj & x ) const
		{
			static std::hash<HashedObj> hf;
			return hash(x) % theLists.size( );
		}

	public:
	
		/********************************************
		* Function Name  : HashTable
		* Pre-conditions :  int size
		* Post-conditions: none 
		*  
		* This is the main constructor of the Hash Table 
		********************************************/
		HashTable( int size ) : currentSize{ 0 }{ 
			theLists.resize( size ); 
		}

		/********************************************
		* Function Name  : contains
		* Pre-conditions :  const HashedObj & x 
		* Post-conditions: bool
		*  
		********************************************/
		bool contains( const HashedObj & x ) const
		{
			auto & whichList = theLists[ myhash( x ) ];
			return find( begin( whichList ), end( whichList ), x ) != end( whichList );
		}

		/********************************************
		* Function Name  : makeEmpty
		* Pre-conditions :  
		* Post-conditions: none
		*  
		********************************************/
		void makeEmpty( )
		{
			for( auto & thisList : theLists )
				thisList.clear( );
		}

		/********************************************
		* Function Name  : insert
		* Pre-conditions :  const HashedObj & x 
		* Post-conditions: bool
		*  
		********************************************/
		bool insert( const HashedObj & x )
		{
			auto & whichList = theLists[ myhash( x ) ];
			if( find( begin( whichList ), end( whichList ), x ) != end( whichList) )
				return false;
			whichList.push_back( x );

				// Rehash; see Section 5.5
			if( ++currentSize > theLists.size( ) )
				rehash( );

			return true;
		}
		
		/********************************************
		* Function Name  : insert
		* Pre-conditions :  HashedObj && x 
		* Post-conditions: bool
		*  
		********************************************/
		bool insert( HashedObj && x )
		{
			auto & whichList = theLists[ myhash( x ) ]; 
			
			if( find( begin( whichList ), end( whichList ), x ) != end( whichList ) )
				return false;
			
			whichList.push_back( std::move( x ) );

			//if( ++currentSize > theLists.size( ) )
				//rehash( );

			return true;
		}

		/********************************************
		* Function Name  : remove
		* Pre-conditions :  const HashedObj & x 
		* Post-conditions: bool
		* 
		* Finds the element in the Hash and removes it 
		* from the appropriate list.
		********************************************/
		bool remove( const HashedObj & x )
		{
			auto & whichList = theLists[ myhash( x ) ];
			auto itr = find( begin( whichList ), end( whichList ), x );

			if( itr == end( whichList ) )
				return false;

			whichList.erase( itr );
			--currentSize;
			return true;
		}
		
		std::ostream& printHash(std::ostream &out){
			
			for(int i = 0; i < theLists.size(); i++){
				
				out << i << " -> ";
				
				//typename std::list<HashedObj>::iterator ptr;

				for( auto ptr = theLists[i].begin(); ptr != theLists[i].end(); ptr++){
					out << *ptr << " ";
				}
				
				out << std::endl;
			}
			
			return out;
			
		}

};

#endif
