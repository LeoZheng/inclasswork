/**********************************************
* File: AVLTree.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
* AVL Tree Class
* Built from the description in the texbook:
* M. A. Weiss, Data structures and algorithm analysis in C++. Boston: Pearson, 2006.
**********************************************/

#ifndef AVL_TREE_H
#define AVL_TREE_H

#include "dsexceptions.h"
#include <algorithm>
#include <iostream> 
using namespace std;

template <typename T>
class AVLTree
{
  public:
    /********************************************
    * Function Name  : AVLTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    AVLTree( ) : root{ nullptr }
      { }
    
    /********************************************
    * Function Name  : AVLTree
    * Pre-conditions :  const AVLTree & rhs 
    * Post-conditions: none
    * 
	* Deep copy
    ********************************************/
    AVLTree( const AVLTree & rhs ) : root{ nullptr }
    {
        root = clone( rhs.root );
    }

    /********************************************
    * Function Name  : AVLTree
    * Pre-conditions :  AVLTree && rhs 
    * Post-conditions: none
    * 
	* Move Constructor
    ********************************************/
    AVLTree( AVLTree && rhs ) : root{ rhs.root }
    {
        rhs.root = nullptr;
    }
    
    /********************************************
    * Function Name  : ~AVLTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    ~AVLTree( )
    {
        makeEmpty( );
    }

    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  const AVLTree & rhs 
    * Post-conditions: AVLTree &
    * 
	* Deep Copy operator
    ********************************************/
    AVLTree & operator=( const AVLTree & rhs )
    {
        AVLTree copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
        
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  AVLTree && rhs 
    * Post-conditions: AVLTree &
    * 
	* Move operator 
    ********************************************/
    AVLTree & operator=( AVLTree && rhs )
    {
        std::swap( root, rhs.root );
        
        return *this;
    }
    
    /********************************************
    * Function Name  : findMin
    * Pre-conditions :  
    * Post-conditions: const T &
	*
    * Find the smallest item in the tree.
    * Throw UnderflowException if empty.  
    ********************************************/
    const T & findMin( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMin( root )->element;
    }

    /********************************************
    * Function Name  : findMax
    * Pre-conditions :  
    * Post-conditions: const T &
    * Find the largest item in the tree.
    * Throw UnderflowException if empty.  
    ********************************************/
    const T & findMax( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };
        return findMax( root )->element;
    }

    /********************************************
    * Function Name  : contains
    * Pre-conditions :  const T & x 
    * Post-conditions: bool
    * 
	* Returns true if x is found in the tree.
    ********************************************/
    bool contains( const T & x ) const
    {
        return contains( x, root );
    }

    /********************************************
    * Function Name  : isEmpty
    * Pre-conditions :  
    * Post-conditions: bool
    * 
	* Test if the tree is logically empty.
    * Return true if empty, false otherwise.	
    ********************************************/
    bool isEmpty( ) const
    {
        return root == nullptr;
    }

    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  
    * Post-conditions: none
    * 
	* Print the tree contents in sorted order.
    ********************************************/
    void printTree( ) const
    {
        if( isEmpty( ) )
            cout << "Empty tree" << endl;
        else
            printTree( root );
    }

    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  
    * Post-conditions: none
    * 
	* Make the tree logically empty.
    ********************************************/
    void makeEmpty( )
    {
        makeEmpty( root );
    }

    /********************************************
    * Function Name  : insert
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    * 
	* Insert x into the tree; duplicates are ignored.
    ********************************************/
    void insert( const T & x )
    {
        insert( x, root );
    }
     
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  T && x 
    * Post-conditions: none
    * 
	* Insert x into the tree; duplicates are ignored.
    ********************************************/
    void insert( T && x )
    {
        insert( std::move( x ), root );
    }
     
    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    * 
	* Remove x from the tree. Nothing is done if x is not found.
    ********************************************/
    void remove( const T & x )
    {
        remove( x, root );
    }
	
	T& getRoot(){
		return root->element;
	}

  private:
    struct AVLNode
    {
        T element;
        AVLNode   *left;
        AVLNode   *right;
        int       height;

        /********************************************
       * Function Name  : AVLNode
       * Pre-conditions :  const T & ele, AVLNode *lt, AVLNode *rt, int h = 0 
       * Post-conditions: none
       * 
		* AVLNode constructor
       ********************************************/
        AVLNode( const T & ele, AVLNode *lt, AVLNode *rt, int h = 0 )
          : element{ ele }, left{ lt }, right{ rt }, height{ h } { }
        
        /********************************************
       * Function Name  : AVLNode
       * Pre-conditions :  T && ele, AVLNode *lt, AVLNode *rt, int h = 0 
       * Post-conditions: none
       * 
		* AVLNode move constructor 
       ********************************************/
        AVLNode( T && ele, AVLNode *lt, AVLNode *rt, int h = 0 )
          : element{ std::move( ele ) }, left{ lt }, right{ rt }, height{ h } { }
    };

    AVLNode *root;

	/********************************************
	* Function Name  : insert
	* Pre-conditions :  const T & x, AVLNode * & t 
	* Post-conditions: `    void
    * Internal method to insert into a subtree.
    * x is the item to insert.
    * t is the node that roots the subtree.
    * Set the new root of the subtree.  
	********************************************/
    void insert( const T & x, AVLNode * & t )
    {
        if( t == nullptr )
            t = new AVLNode{ x, nullptr, nullptr };
	else if( x == t->element){
		// Do nothing. No duplicates.
	}
	else if( x < t->element )
            insert( x, t->left );
        else if( t->element < x )
            insert( x, t->right );
        
        balance( t );
    }

    /********************************************
    * Function Name  : insert
    * Pre-conditions :  T && x, AVLNode * & t 
    * Post-conditions: none
    * Internal method to insert into a subtree.
    * x is the item to insert.
    * t is the node that roots the subtree.
    * Set the new root of the subtree.  
    ********************************************/
    void insert( T && x, AVLNode * & t )
    {
        if( t == nullptr )
            t = new AVLNode{ std::move( x ), nullptr, nullptr };
        else if( x < t->element )
            insert( std::move( x ), t->left );
        else if( t->element < x )
            insert( std::move( x ), t->right );
        
        balance( t );
    }
     
    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x, AVLNode * & t 
    * Post-conditions: none
    * Internal method to remove from a subtree.
    * x is the item to remove.
    * t is the node that roots the subtree.
    * Set the new root of the subtree.  
    ********************************************/
    void remove( const T & x, AVLNode * & t )
    {
        if( t == nullptr )
            return;   // Item not found; do nothing
        
        if( x < t->element )
            remove( x, t->left );
        else if( t->element < x )
            remove( x, t->right );
        else if( t->left != nullptr && t->right != nullptr ) // Two children
        {
            t->element = findMin( t->right )->element;
            remove( t->element, t->right );
        }
        else
        {
            AVLNode *oldNode = t;
            t = ( t->left != nullptr ) ? t->left : t->right;
            delete oldNode;
        }
        
        balance( t );
    }
    
    static const int ALLOWED_IMBALANCE = 1;

    /********************************************
    * Function Name  : balance
    * Pre-conditions :  AVLNode * & t 
    * Post-conditions: none
	*
    * Assume t is balanced or within one of being balanced
    ********************************************/
    void balance( AVLNode * & t )
    {
        if( t == nullptr )
            return;
        
        if( height( t->left ) - height( t->right ) > ALLOWED_IMBALANCE )
            if( height( t->left->left ) >= height( t->left->right ) )
                rotateWithLeftChild( t );
            else
                doubleWithLeftChild( t );
        else
        if( height( t->right ) - height( t->left ) > ALLOWED_IMBALANCE )
            if( height( t->right->right ) >= height( t->right->left ) )
                rotateWithRightChild( t );
            else
                doubleWithRightChild( t );
                
        t->height = max( height( t->left ), height( t->right ) ) + 1;
    }
    
    /********************************************
    * Function Name  : findMin
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: AVLNode *
	*
    * Internal method to find the smallest item in a subtree t.
    * Return node containing the smallest item. 
    ********************************************/
    AVLNode * findMin( AVLNode *t ) const
    {
        if( t == nullptr )
            return nullptr;
        if( t->left == nullptr )
            return t;
        return findMin( t->left );
    }

    /********************************************
    * Function Name  : findMax
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: AVLNode *
    * 
	* Internal method to find the largest item in a subtree t.
    * Return node containing the largest item.
    ********************************************/
    AVLNode * findMax( AVLNode *t ) const
    {
        if( t != nullptr )
            while( t->right != nullptr )
                t = t->right;
        return t;
    }

    /********************************************
    * Function Name  : contains
    * Pre-conditions :  const T & x, AVLNode *t 
    * Post-conditions: bool
    * 
	* Internal method to test if an item is in a subtree.
    * x is item to search for.
    * t is the node that roots the tree.
    ********************************************/
    bool contains( const T & x, AVLNode *t ) const
    {
        if( t == nullptr )
            return false;
        else if( x < t->element )
            return contains( x, t->left );
        else if( t->element < x )
            return contains( x, t->right );
        else
            return true;    // Match
    }

    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  AVLNode * & t 
    * Post-conditions: none
    * 
	* Internal method to make subtree empty.
    ********************************************/
    void makeEmpty( AVLNode * & t )
    {
        if( t != nullptr )
        {
            makeEmpty( t->left );
            makeEmpty( t->right );
            delete t;
        }
        t = nullptr;
    }

    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: none
    * 
	* Internal method to print a subtree rooted at t in sorted order.
    ********************************************/
    void printTree( AVLNode *t ) const
    {
        if( t != nullptr )
        {
            printTree( t->left );
            cout << t->element << endl;
            printTree( t->right );
        }
    }

    /********************************************
    * Function Name  : clone
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: AVLNode *
    * 
	* Internal method to clone subtree.
    ********************************************/
    AVLNode * clone( AVLNode *t ) const
    {
        if( t == nullptr )
            return nullptr;
        else
            return new AVLNode{ t->element, clone( t->left ), clone( t->right ), t->height };
    }
        // Avl manipulations

    /********************************************
    * Function Name  : height
    * Pre-conditions :  AVLNode *t 
    * Post-conditions: int
    * 
	* Return the height of node t or -1 if nullptr.
    ********************************************/
    int height( AVLNode *t ) const
    {
        return t == nullptr ? -1 : t->height;
    }

    /********************************************
    * Function Name  : max
    * Pre-conditions :  int lhs, int rhs 
    * Post-conditions: int
    * 
	* Return the maximum of the left or right node 
    ********************************************/
    int max( int lhs, int rhs ) const
    {
        return lhs > rhs ? lhs : rhs;
    }

    /********************************************
    * Function Name  : rotateWithLeftChild
    * Pre-conditions :  AVLNode * & k2 
    * Post-conditions: none
    * 
	* Rotate binary tree node with left child.
    * For AVL trees, this is a single rotation for case 1.
    * Update heights, then set new root.
    ********************************************/
    void rotateWithLeftChild( AVLNode * & k2 )
    {
        AVLNode *k1 = k2->left;
        k2->left = k1->right;
        k1->right = k2;
        k2->height = max( height( k2->left ), height( k2->right ) ) + 1;
        k1->height = max( height( k1->left ), k2->height ) + 1;
        k2 = k1;
    }

    /********************************************
    * Function Name  : rotateWithRightChild
    * Pre-conditions :  AVLNode * & k1 
    * Post-conditions: none
    * 
	* Rotate binary tree node with right child.
    * For AVL trees, this is a single rotation for case 4.
    * Update heights, then set new root.
    ********************************************/
    void rotateWithRightChild( AVLNode * & k1 )
    {
        AVLNode *k2 = k1->right;
        k1->right = k2->left;
        k2->left = k1;
        k1->height = max( height( k1->left ), height( k1->right ) ) + 1;
        k2->height = max( height( k2->right ), k1->height ) + 1;
        k1 = k2;
    }

    /********************************************
    * Function Name  : doubleWithLeftChild
    * Pre-conditions :  AVLNode * & k3 
    * Post-conditions: none
    * 
	* Double rotate binary tree node: first left child.
    * with its right child; then node k3 with new left child.
    * For AVL trees, this is a double rotation for case 2.
    * Update heights, then set new root.
    ********************************************/
    void doubleWithLeftChild( AVLNode * & k3 )
    {
        rotateWithRightChild( k3->left );
        rotateWithLeftChild( k3 );
    }

    /********************************************
    * Function Name  : doubleWithRightChild
    * Pre-conditions :  AVLNode * & k1 
    * Post-conditions: none
    * Double rotate binary tree node: first right child.
    * with its left child; then node k1 with new right child.
    * For AVL trees, this is a double rotation for case 3.
    * Update heights, then set new root. 
    ********************************************/
    void doubleWithRightChild( AVLNode * & k1 )
    {
        rotateWithLeftChild( k1->right );
        rotateWithRightChild( k1 );
    }
};

#endif
