/**********************************************                                                                      
 * File: cycle.cpp                                                                                                     
 * Author: Tianze Zheng                                                                                          
 * Email: tzheng2@nd.edu                                                                                       
 *                                                                                                                   
 * Contains an STL implementation of an Adjacency List                                                               
 * graph Depth-First Search (no directions or weights)                                          
 *                                                                                                                   
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ g++ -g -std=c++11 -Wpedantic cycle.cpp -o cycle           
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ ./cycle                                                       
                                                                                                                     
Output may be found at cycle.out. Can be generated using ./cycle > cycle.out                                           
                                                                                                                     
**********************************************/

#include<iostream> 
#include <list> 
#include <limits.h> 
  
class Graph 
{ 
  int V;    // No. of vertices 
  std::list<int> *adj;    // Pointer to an array containing adjacency lists 
  bool isCyclicRec(int v, bool visited[], bool *rs);  // used by isCyclic() 
public: 
  Graph(int V);   // Constructor 
  void addEdge(int v, int w);   // to add an edge to graph 
  bool isCyclic();    // returns true if there is a cycle in this graph 
}; 

/********************************************                                                                         
 * Function Name  : constructor                                                                                    
 * Pre-conditions : int V                                                                                      
 * Post-conditions: none                                                                                              
 *                                                                                                                    
 * initialization of the class Graph with the                                                                  
 * assigned list
 ********************************************/  
Graph::Graph(int V) 
{ 
  this->V = V; 
  adj = new std::list<int>[V]; 
} 

/********************************************                                                                         
 * Function Name  : addEdge                                                                                           
 * Pre-conditions : int v, int w                                                                                      
 * Post-conditions: none                                                                                              
 *                                                                                                                    
 * Insert an edge into the graph                                                                                      
 ********************************************/
void Graph::addEdge(int v, int w) 
{ 
  adj[v].push_back(w);
} 

/********************************************                                                                         
 * Function Name  : isCyclicRec                                                                                     
 * Pre-conditions : int v, bool visited[], bool *recStack                                                          
 * Post-conditions: none                                                                                              
 *                                                                                                                    
 * Recursively iterate through the directed graph
 ********************************************/
bool Graph::isCyclicRec(int v, bool visited[], bool *recStack) 
{ 
  if(visited[v] == false) 
    { 
      // Mark the current node as visited and part of recursion stack 
      visited[v] = true; 
      recStack[v] = true; 
  
      // Recur for all the vertices adjacent to this vertex 
      std::list<int>::iterator i; 
      for(i = adj[v].begin(); i != adj[v].end(); ++i) 
        { 
	  if ( !visited[*i] && isCyclicRec(*i, visited, recStack) ) 
	    return true; 
	  else if (recStack[*i]) 
	    return true; 
        } 
  
    } 
  recStack[v] = false;  // remove the vertex from recursion stack 
  return false; 
} 

/********************************************                                                                          
 * Function Name  : isCyclicRec                                                                                        
 * Pre-conditions : int v, bool visited[], bool *recStack                                                              
 * Post-conditions: none                                                                                               
 *                                                                                                                    
 * Returns true if the graph contains a cycle, else false
 ********************************************/
bool Graph::isCyclic() 
{ 
  // Mark all the vertices as not visited and not part of recursion 
  // stack 
  bool *visited = new bool[V]; 
  bool *recStack = new bool[V]; 
  for(int i = 0; i < V; i++) 
    { 
      visited[i] = false; 
      recStack[i] = false; 
    } 
  
  // Call the recursive helper function to detect cycle in different 
  // DFS trees 
  for(int i = 0; i < V; i++) 
    if (isCyclicRec(i, visited, recStack)) 
      return true; 
  
  return false; 
} 

/********************************************                                                                          
 * Function Name  : isCyclicRec                                                                                       
 * Pre-conditions : int v, bool visited[], bool *recStack                                                              
 * Post-conditions: none                                                                                               
 *                                                                                                                     
 * Main function
 ********************************************/
int main() 
{ 
  // Create a graph given in the above diagram 
  
  Graph g1(100); 
  g1.addEdge(0, 1); 
  g1.addEdge(0, 4); 
  g1.addEdge(1, 4); 
  g1.addEdge(1, 2); 
  g1.addEdge(1, 3); 
  g1.addEdge(2, 3); 
  g1.addEdge(3, 4);
  
  Graph g2(100);
  g2.addEdge(4, 20);
  g2.addEdge(5, 18);
  g2.addEdge(5, 20);
  g2.addEdge(11, 11);
  g2.addEdge(20, 30);
  g2.addEdge(20, 11);
  g2.addEdge(30, 5);
  
  Graph g3(100);
  g3.addEdge('N', 'O');
  g3.addEdge('N', 'T');
  g3.addEdge('O', 'R');
  g3.addEdge('O', 'E');
  g3.addEdge('T', 'R');
  g3.addEdge('R', 'D');
  g3.addEdge('E', 'D');
  g3.addEdge('D', 'R');

  if(g1.isCyclic()) 
    std::cout << "Graph contains cycle\n"; 
  else
    std::cout << "Graph doesn't contain cycle\n"; 
  
  if(g2.isCyclic())
    std::cout << "Graph contains cycle\n";
  else
    std::cout << "Graph doesn't contain cycle\n";
  
  if(g3.isCyclic())
    std::cout << "Graph contains cycle\n";
  else
    std::cout << "Graph doesn't contain cycle\n";


  return 0; 
}
