/**********************************************
* File: factTail.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
*  Solves factorial with Tail Recursion
**********************************************/
#include "Supp.h"
#include "Recurse.h"

int main(int argc, char**argv){
	
	std::cout << "Tail Recursion Result: " << factTail(getArgv1Num(argc, argv)) << std::endl;
	
	return 0;
	
}
