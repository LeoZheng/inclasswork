/**********************************************
* File: TicTac.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is the driver function for the Tic Tac Toe problem 
* Compilation: g++ -g -std=c++11 -Wpedantic TicTac.cpp -o TicTac
* Running: ./TicTac 
**********************************************/
#include <iostream>
#include <unordered_map>
#include <string>
#include <cmath>

/********************************************
* Function Name  : printBoard
* Pre-conditions : int** TicTacBoard[
* Post-conditions: none
* 
* Takes in a 3x3 board and prints to the 
* std::cout stream 
********************************************/
void printBoard(int* TicTacBoard){
	for(int i = 0; i < 3; i++){
		for(int j = 0; j < 3; j++){
			std::cout << "|" << TicTacBoard[3*i+j];
		}
		std::cout << "|" << std::endl;
	}
}

/********************************************
* Function Name  : printBoard2
* Pre-conditions : int* TicTacBoard
* Post-conditions: none
* 
* Test function to print the board. Truncated 
********************************************/
void printBoard2(int* TicTacBoard){
	for(int i = 0; i < 9; i++){
		std::cout << "|" << TicTacBoard[i];
	}
	std::cout << "|" << std::endl;
}

/********************************************
* Function Name  : boardToStr
* Pre-conditions : int* TicTacBoard
* Post-conditions: std::string
* 
* Converts the integer board to a string 
********************************************/
std::string boardToStr(int* TicTacBoard){
	
	std::string theBoard;
	
	for(int i = 0; i < 9; i++){
		theBoard += std::to_string(TicTacBoard[i]);
	}
	
	return theBoard;
	
}


/********************************************
* Function Name  : hasWon
* Pre-conditions : int* TicTacBoard
* Post-conditions: bool
* 
* Determines if the board has a winner 
********************************************/
bool hasWon(int* TicTacBoard){
	
	// Check Horizontal
	if(TicTacBoard[0] != 0 && (TicTacBoard[0] == TicTacBoard[1]) && (TicTacBoard[0] == TicTacBoard[2]))
		return true;
	
	else if(TicTacBoard[3] != 0 && (TicTacBoard[3] == TicTacBoard[4]) && (TicTacBoard[3] == TicTacBoard[5]))
		return true;
	
	else if(TicTacBoard[6] != 0 && (TicTacBoard[6] == TicTacBoard[7]) && (TicTacBoard[6] == TicTacBoard[8]))
		return true;
	
	// Check Vertical
	else if(TicTacBoard[0] != 0 && (TicTacBoard[0] == TicTacBoard[3]) && (TicTacBoard[0] == TicTacBoard[6]))
		return true;

	else if(TicTacBoard[1] != 0 && (TicTacBoard[1] == TicTacBoard[4]) && (TicTacBoard[1] == TicTacBoard[7]))
		return true;
	
	else if(TicTacBoard[2] != 0 && (TicTacBoard[2] == TicTacBoard[5]) && (TicTacBoard[2] == TicTacBoard[8]))
		return true;
	
	// Check Diagonal
	else if(TicTacBoard[0] != 0 && (TicTacBoard[0] == TicTacBoard[4]) && (TicTacBoard[0] == TicTacBoard[8]))
		return true;

	else if(TicTacBoard[2] != 0 && (TicTacBoard[2] == TicTacBoard[4]) && (TicTacBoard[2] == TicTacBoard[6]))
		return true;	
	
	return 0;
}

/********************************************
* Function Name  : populateSolutions
* Pre-conditions : int* TicTacBoard, std::unordered_map<int*, bool>* HashBoard
* Post-conditions: none
* 
* Populates the Hash Table with Board information 
********************************************/
void populateSolutions(int* TicTacBoard, std::unordered_map<std::string, bool>* HashBoard){ 

	for(int iter = 0; iter < pow(3, 9); iter++){
		if(TicTacBoard[8] != 2)
			TicTacBoard[8]++;
		else{
			TicTacBoard[8] = 0;
			
			// Carry Arithmetic
			for(int i = 7; i >= 0; i--){
				
				// Carry Required
				if(TicTacBoard[i] == 2)
					TicTacBoard[i] = 0;
				
				// No Carry Required 
				else{
					TicTacBoard[i]++;
					i = -1;
				}
			}
		}	
		
		//printBoard2(TicTacBoard);
		
		HashBoard->insert( {boardToStr(TicTacBoard), hasWon(TicTacBoard)} );
	}

}

/********************************************
* Function Name  : getBoardWinner
* Pre-conditions : int* TicTacHasWinner
* Post-conditions: none
* 
* A function that generates a winning testcase 
********************************************/
void getBoardWinner(int* TicTacHasWinner){
	TicTacHasWinner[0] = 2; TicTacHasWinner[1] = 1; TicTacHasWinner[2] = 0;
	TicTacHasWinner[3] = 0; TicTacHasWinner[4] = 2; TicTacHasWinner[5] = 1;
	TicTacHasWinner[6] = 0; TicTacHasWinner[7] = 1; TicTacHasWinner[8] = 2;
}

/********************************************
* Function Name  : getBoardNoWinner
* Pre-conditions : int* TicTacNoWinner
* Post-conditions: none
* 
* A function that generates a test case with no winner 
********************************************/
void getBoardNoWinner(int* TicTacNoWinner){
	TicTacNoWinner[0] = 2; TicTacNoWinner[1] = 1; TicTacNoWinner[2] = 2;
	TicTacNoWinner[3] = 1; TicTacNoWinner[4] = 2; TicTacNoWinner[5] = 2;
	TicTacNoWinner[6] = 1; TicTacNoWinner[7] = 2; TicTacNoWinner[8] = 1;
}

/********************************************
* Function Name  : checkWinner
* Pre-conditions : int* TicTac, std::unordered_map<std::string, bool>* HashBoard
* Post-conditions: none
* 
* Prints the board and then calls the Hash Function to see if the case is
* a winner or not. Prints to the user 
********************************************/
void checkWinner(int* TicTac, std::unordered_map<std::string, bool>* HashBoard){
	printBoard(TicTac);
	
	if((*HashBoard)[boardToStr(TicTac)])
		std::cout << "Board Contains a winner!" << std::endl;
	else
		std::cout << "Board does not contain a winner!" << std::endl;
	std::cout << std::endl;
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* The main driver function for the Tic Tac Toe
* solver problem 
********************************************/
int main(int argc, char** argv){

	std::unordered_map<std::string, bool> HashBoard;
	
	// Allocate the memory for the TicTacToe Board 
	int* TicTacBoard;
	TicTacBoard = (int *) calloc (9, sizeof(int));

	// Populate the Hash Table with all possible outcomes
	populateSolutions(TicTacBoard, &HashBoard); 

	// Create a Case with a Winner
	int* TicTacHasWinner;
	TicTacHasWinner = (int *) calloc (9, sizeof(int));
	getBoardWinner(TicTacHasWinner);
	checkWinner(TicTacHasWinner, &HashBoard);
	
	// Create a Case with no Winner	
	int* TicTacNoWinner;
	TicTacNoWinner = (int *) calloc (9, sizeof(int));
	getBoardNoWinner(TicTacNoWinner);
	checkWinner(TicTacNoWinner, &HashBoard);
	
	// Delete the Tic Tac Toe Board
	delete[] TicTacBoard;
	delete[] TicTacHasWinner;
	delete[] TicTacNoWinner;

	return 0;
}

